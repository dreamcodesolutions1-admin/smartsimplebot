<?php
error_reporting(E_ERROR);

include ('../app/Config.php');
include ('../app/Core.php');
include ('../app/3CommasConnector.php');
include ('../app/DataMapper.php');
include ('../app/DataReader.php');
include ('../app/functions.php');

$action = $_REQUEST['action'];

if (empty($action)) {
    echo '<h1>SmartSimpleBot Update - Strategy Management</h1>';
    echo 'Click <a href="v1_1.php?action=start">this link</a> to start updating.';
}
else if ($action == 'start') {
    $dataMapper = new DataMapper();
    $dataReader = new DataReader();

    // Create a strategy table
    $strategies_exist = $dataMapper->dbh->prepare('SHOW tables like "strategies"');
    $strategies_exist->execute();
    $strategies_exist_res = $strategies_exist->fetch(PDO::FETCH_ASSOC);

    if (!$strategies_exist_res) {
        $create_strategies_table = '
            SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
            SET AUTOCOMMIT = 0;
            START TRANSACTION;
            SET time_zone = "+00:00";

            CREATE TABLE strategies (
                strategy_id int(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name text NOT NULL,
                description text NULL,
                active tinyint NOT NULL default 1,
                created_by int(12) NOT NULL,
                last_updated timestamp NOT NULL default current_timestamp()
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            
            COMMIT;
        ';

        $stmt = $dataMapper->dbh->prepare($create_strategies_table);
        $stmt->execute();
        $stmt = null;
    }

    // Create bot table
    $bot_exists = $dataMapper->dbh->prepare('SHOW tables like "bots"');
    $bot_exists->execute();
    $bot_exists_res = $bot_exists->fetch(PDO::FETCH_ASSOC);

    if (!$bot_exists_res) {
        $create_bot_table = '
            SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
            SET AUTOCOMMIT = 0;
            START TRANSACTION;
            SET time_zone = "+00:00";

            CREATE TABLE bots (
                account_bot_id int(12) NOT NULL,
                account_id int(12) NOT NULL,
                name text NOT NULL,
                pairs text NOT NULL,
                strategy_id int(12),
                last_updated timestamp
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
            
            ALTER TABLE bots ADD PRIMARY KEY (account_bot_id);
            CREATE INDEX bot_strategy ON bots(strategy_id);

            COMMIT;
        ';

        $stmt = $dataMapper->dbh->prepare($create_bot_table);
        $stmt->execute();
        $stmt = null;
    }

    // Fetch all bots from 3Commas & populate them in our local DB.
    $all_accounts = $dataReader->get_all_accounts();
    foreach($all_accounts as $account_wrapper) {
        $account_info = $dataReader->get_account_info_internal($account_wrapper['internal_account_id']);
        $xcommas = new MC3Commas\threeCommas(BASE_URL , $account_info['api_key'] , $account_info['api_secret']);
        $bots = $xcommas->get_all_bots(['account_id' => $account_info['bot_account_id'] , 'limit' => 100]);

        foreach ((array)$bots as $bot) {
            $update_or_create_bot = '
                INSERT INTO bots (account_bot_id, account_id, name, pairs) 
                VALUES (:account_bot_id, :account_id, :name, :pairs) 
                ON DUPLICATE KEY UPDATE
                account_id = :account_id, name = :name, pairs = :pairs
            ';
            $stmt = $dataMapper->dbh->prepare($update_or_create_bot);
            $stmt->bindParam(':account_id', $account_info['bot_account_id']);
            $stmt->bindParam(':account_bot_id', $bot['id']);
            $stmt->bindParam(':name', $bot['name']);
            $stmt->bindParam(':pairs', $bot['pairs'][0]);
            $stmt->execute();
            $stmt = null;
        }
    }

    echo '<h1>SmartSimpleBot Update - Strategy Management</h1>';
    echo '<h2>Update completed. Login <a href="admin/index.php">on the admin homepage.</a> </h2>';
}
?>