<?php
error_reporting(E_ERROR);

include ('../app/Config.php');
include ('../app/Core.php');
include ('../app/3CommasConnector.php');
include ('../app/DataMapper.php');
include ('../app/DataReader.php');
include ('../app/functions.php');

function createOrUpdateTable($dataMapper, $table_name, $create_query) {
    // Create TV alert data table
    $table_exists = $dataMapper->dbh->prepare("SHOW tables like \"$table_name\"");
    $table_exists->execute();
    $table_exists_res = $table_exists->fetch(PDO::FETCH_ASSOC);

    if (!$table_exists_res) {
        $stmt = $dataMapper->dbh->prepare($create_query);
        $stmt->execute();
        $stmt = null;
    }
}

$action = $_REQUEST['action'];

if (empty($action)) {
    echo '<h1>SmartSimpleBot Update - Rule Engine V0.1</h1>';
    echo 'Click <a href="v1_2.php?action=start">this link</a> to start updating.';
}
else if ($action == 'start') {
    $dataMapper = new DataMapper();
    $dataReader = new DataReader();

    $create_tv_alert_table = '
        SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
        SET AUTOCOMMIT = 0;
        START TRANSACTION;
        SET time_zone = "+00:00";

        CREATE TABLE tv_alert_data (
            id int(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            rule_key varchar(20),
            query_rule_id int(12),
            pair varchar(20),
            timeframe varchar(10),
            payload TEXT
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
        COMMIT;
    ';
    createOrUpdateTable($dataMapper, 'tv_alert_data', $create_tv_alert_table);

    $create_rule_table = '
        SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
        SET AUTOCOMMIT = 0;
        START TRANSACTION;
        SET time_zone = "+00:00";

        CREATE TABLE rules (
            id int(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name TEXT NOT NULL,
            rule_key varchar(20),
            description TEXT,
            command TEXT,
            created_at timestamp NOT NULL default current_timestamp()
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
        COMMIT;
    ';
    createOrUpdateTable($dataMapper, 'rules', $create_rule_table);

    $create_queries_table = '
        SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
        SET AUTOCOMMIT = 0;
        START TRANSACTION;
        SET time_zone = "+00:00";

        CREATE TABLE queries (
            id int(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            rule_id int(12),
            param TEXT,
            value TEXT,
            timeframe varchar(10),
            interval int,
            last_run timestamp
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
        COMMIT;
    ';
    createOrUpdateTable($dataMapper, 'queries', $create_queries_table);

    $create_scans_table = '
        SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
        SET AUTOCOMMIT = 0;
        START TRANSACTION;
        SET time_zone = "+00:00";

        CREATE TABLE scans (
            id int(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            created_at NOT NULL default current_timestamp(),
            started_at timestamp,
            completed_at timestamp
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
        COMMIT;
    ';
    createOrUpdateTable($dataMapper, 'scans', $create_scans_table);

    $create_scan_queries_table = '
        SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
        SET AUTOCOMMIT = 0;
        START TRANSACTION;
        SET time_zone = "+00:00";

        CREATE TABLE scan_queries (
            id int(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            scan_id int(12),
            query_id int(12)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
        COMMIT;
    ';
    createOrUpdateTable($dataMapper, 'scan_queries', $create_scan_queries_table);

    $create_scan_result_table = '
        SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
        SET AUTOCOMMIT = 0;
        START TRANSACTION;
        SET time_zone = "+00:00";

        CREATE TABLE scan_results (
            id int(12) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            scan_id int(12),
            pair_id int(12),
            tv_indicator_data_id int(12),
            tv_alert_data_id int(12)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
        COMMIT;
    ';
    createOrUpdateTable($dataMapper, 'scan_queries', $create_scan_result_table);

    echo '<h1>SmartSimpleBot Update - Strategy Management</h1>';
    echo '<h2>Update completed. Login <a href="admin/index.php">on the admin homepage.</a> </h2>';
}
?>