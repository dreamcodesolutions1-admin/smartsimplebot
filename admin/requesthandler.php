<?php
error_reporting(E_ERROR);
// We need to use sessions, so you should always start sessions using the below code.
session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
	header('Location: index.php?response=notloggedin');
	die;
}

// If using Sentry & Log, must include this first
include ('../vendor/autoload.php');

include ('../app/Sentry.php');
include ('../app/Config.php');
include ('../app/Core.php');
include ('../app/3CommasConnector.php');
include ('../app/DataMapper.php');
include ('../app/DataReader.php');
include ('../app/Table.php');
include ('../app/functions.php');

$dataMapper = new DataMapper();
$dataReader = new DataReader();

$action = $_REQUEST['action'];

/**
 * Load all accounts for current user
 */
if($action == 'load_all_accounts') {

    $accounts = $dataReader->get_user_accounts($_SESSION['user_id']);

    // Terminate if the user is nog logged in
    check_credentials($_SESSION['user_id']);

    $account_response = [];

    $i = 0;
    foreach ($accounts as $account) {

        $settings = $dataReader->get_account_settings($account['internal_account_id']);

        $account_info = $dataReader->get_account_info_internal($account['internal_account_id']);
        $xcommas = new MC3Commas\threeCommas(BASE_URL , $account_info['api_key'] , $account_info['api_secret']); 

        try {
            $stats = $xcommas->get_bot_stats(['account_id' => $account['bot_account_id']]);
            $deals = $xcommas->get_deals(['account_id' => $account['bot_account_id'] , 'scope' => 'active']);

            if (isset($stats['today_stats']['USDT'])) {
                $stats_usdt = '$ '.number_format($stats['today_stats']['USDT'],2);
            }
            else {
                $stats_usdt = '$ 0.00';
            }
            
            $deals_running = count((array)$deals);
        } catch (Exception $e) {

            $stats_usdt = '-error-';
            $deals_running = '-error-';
        } 
      
        $account_response[$i]['internal_id'] = $account['internal_account_id'];
        $account_response[$i]['3c_id'] = $account['bot_account_id'];
        $account_response[$i]['internal_name'] = $account['account_name'];
        $account_response[$i]['mad'] = $settings['max_active_deals'];
        $account_response[$i]['bo_size'] = $settings['bo_size'];
        $account_response[$i]['active'] = $settings['active'];
        $account_response[$i]['daily_usdt_profit'] = $stats_usdt;
        $account_response[$i]['open_deals'] = $deals_running;
        $account_response[$i]['tv_screener_timeframe'] = $settings['tv_screener_timeframe'];
 
        $i++;
    }

    $table = new STable();
    $table->class = 'table table-hover table-striped table-bordered';
    $table->width = '100%';

    $table->thead()
        ->th('Account ID')
        ->th('Name')
        ->th('Max deals')
        ->th('BO/SO Size')
        ->th('Manage bots')
        ->th('Status')
        ->th('TV Alerts')
        ->th('Telegram')
        ->th('Logs')
        ->th('Delete')
        ->th('Panic')
        ->th('Screener Timeframe')
        ->th('Open deals')
        ->th('Daily profit');
        
   
    foreach ($account_response as $response) {

        if($response['active'] == 1) {
            $switch = '<a class="disable_account_link" id="account_'.$response['internal_id'].'"><i class="fas fa-stop-circle"></i> Disable</a>';
        }
        if($response['active'] == 0) {
            $switch = '<a class="enable_account_link" id="account_'.$response['internal_id'].'"><i class="fas fa-play-circle"></i> Enable</a>';
        }

        $timeframes = [
            ['label' => 'Disabled', 'value' => ''],
            ['label' => '5m', 'value' => '5m'],
            ['label' => '15m', 'value' => '15m'],
            ['label' => '1h', 'value' => '1h'],
            ['label' => '4h', 'value' => '4h'],
            ['label' => '1d', 'value' => '1d'],
        ];

        $table->tr()
        ->td($response['3c_id'])
        ->td($response['internal_name'])
        ->td(create_dropdown_number_with_id(0 , 100 , 'mad_dropdown' , 'mad_dropdown' , 'account_'.$response['internal_id'] , $response['mad']))
        ->td(create_dropdown_number_with_id(5 , 200 , 'bo_size' , 'bo_size' , 'account_'.$response['internal_id'] , $response['bo_size']))
        ->td('<a class="manage_bots_link" id="mbots_'.$response['internal_id'].'"><i class="fas fa-robot"></i> Manage bots </a>')
        ->td($switch)
        ->td('<a class="tv_alerts_link" id="account_'.$response['internal_id'].'"><i class="fas fa-chart-bar"></i>  Trading View alerts</a>')
        ->td('<a class="telegram_settings_link" id="account_'.$response['internal_id'].'"><i class="fas fa-comment-dots"></i>  Telegram settings</a> | <a class="test_message_link" id="sentmsg_'.$response['internal_id'].'"><i class="fas fa-paper-plane"></i> Test msg.</a>')
        ->td('<a class="logbook_link" id="account_'.$response['internal_id'].'"><i class="fas fa-book"></i>  Logbook</a>')
        ->td('<a class="delete_account_link" id="account_'.$response['internal_id'].'"><i class="fas fa-trash"></i>  Delete</a>')
        ->td('<a class="cancel_deals_link" id="account_'.$response['internal_id'].'"><i class="fas fa-power-off" style="color: red;"></i>  Stop All Deals</a>')
        ->td(create_customizable_dropdown_options($timeframes, 'tv_screener_timeframe' , 'tv_screener_timeframe' , 'account_'.$response['internal_id'], ['value' => $response['tv_screener_timeframe']]))
        ->td($response['open_deals'])
        ->td($response['daily_usdt_profit']);
    }

    echo $table->getTable();

}

/**
 * Add an account
 */
if($action == 'add_account') {

    $dataMapper->insert_account($_SESSION['user_id'] , $_REQUEST['bot_account_id'] , $_REQUEST['account_name'] , $_REQUEST['api_key'] , $_REQUEST['api_secret']);

    echo 'Account added.';
}

/**
 * Edit an account
 */
if($action == 'edit_account') {

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    $dataMapper->edit_account($_SESSION['user_id'] , $internal_account_id);

    echo 'Account deleted.';
}

/**
 * Delete an account
 */
if($action == 'delete_account') {

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    $dataMapper->delete_account($_SESSION['user_id'] , $internal_account_id);

    echo 'Account deleted.';
}

/**
 * Disable an account
 */
if($action == 'disable_account') {

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    $dataMapper->enable_disable_account($internal_account_id , 0);

    echo 'Account disabled.';
}

/**
 * Enable an account
 */
if($action == 'enable_account') {

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    $dataMapper->enable_disable_account($internal_account_id , 1);

    echo 'Account enabled.';
}

/**
 * Update max acticve deals
 */
if($action == 'update_max_active_deals') {

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    $dataMapper->update_max_active_deals($internal_account_id , $_REQUEST['deals']);

    echo 'Max deals set to '.$_REQUEST['deals'];
}

/**
 * Update Screener timeframe
 */
if ($action == 'update_screener_timeframe') {
    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];
    $timeframe = $_REQUEST['timeframe'];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    check_credentials($account_info['user_id']);

    $dataMapper->update_tv_screener_timeframe($internal_account_id, $timeframe);

    echo 'Screener Timeframe is now '.($timeframe == "" ? '"Disabled"' : '"'.$timeframe.'"');
}

/**
 * Update BO/SO size
 */
if($action == 'update_bo_size') {

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $size = $_REQUEST['size'];

    // Get settings to create 3C connection
    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    // Update 3C , takes a while
    $xcommas = new MC3Commas\threeCommas(BASE_URL , $account_info['api_key'] , $account_info['api_secret']); 
    $bots = $xcommas->get_all_bots(['account_id' => $account_info['bot_account_id'] , 'limit' => 100]);

    foreach ($bots as $bot) {

        $data = [
            'name' => $bot['name'],
            'pairs' => json_encode($bot['pairs']),
            'base_order_volume' => $size,
            'take_profit' => $bot['take_profit'],
            'safety_order_volume' => $size,
            'martingale_volume_coefficient' => $bot['martingale_volume_coefficient'],
            'martingale_step_coefficient' => $bot['martingale_step_coefficient'],
            'max_safety_orders' => $bot['max_safety_orders'],
            'active_safety_orders_count' => $bot['active_safety_orders_count'],
            'safety_order_step_percentage' => $bot['safety_order_step_percentage'],
            'take_profit_type' => $bot['take_profit_type'],
            'trailing_enabled' => $bot['trailing_enabled'],
            'trailing_deviation' => $bot['trailing_deviation'],
            'strategy_list' => json_encode(['strategy' => $bot['strategy_list'][0]['strategy']]),
            'bot_id' => $bot['id'],
            'leverage_type' =>  $bot['leverage_type'] ,
            'leverage_custom_value' => $bot['leverage_custom_value'],
            'start_order_type' => $bot['start_order_type']

        ];
    
        $update = $xcommas->update_bot($bot['id'] , $data);
    } 

    $dataMapper->update_bo_size($internal_account_id , $_REQUEST['size']);

    echo 'Size update to '.$size.'. Please review your settings on 3Commas!';
}

/**
 * Load all bots on account
 */
if($action == 'load_bots') {

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);


    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    $xcommas = new MC3Commas\threeCommas(BASE_URL , $account_info['api_key'] , $account_info['api_secret']); 
    $bots = $xcommas->get_all_bots(['account_id' => $account_info['bot_account_id'] , 'limit' => 100]);

    array_multisort(array_column((array)$bots, 'name'),  SORT_ASC , (array)$bots);

    // Populate dropdown of strategies
    $strategies = $dataReader->get_strategies();
    array_unshift($strategies, ['id' => '', 'name' => '']);

    // Load strategies from local DB
    $local_bots = $dataReader->load_local_bots($account_info['bot_account_id']);

    $table = new STable();
    $table->class = 'table table-hover table-striped table-bordered';
    $table->width = '100%';
    $table->id = 'manage_bots_table';

    echo '<h2> Manage bots </h2>';

    echo '<form method="POST" id="change_bots" onsubmit="return a_change_bots();">';

    echo '<input type="submit" name="submit_form" class="submit_mb" value="Save all changes">';
    echo '<input type="hidden" name="account_id" value="'.$internal_account_id.'"/>';
   
         
    $table->thead()
        ->th('Bot (name):')
        ->th('Pair:')
        ->th('Start condition')
        ->th('Max SO:')
        ->th('Active SO:')
        ->th('BO/SO Type:')
        ->th('BO Size:')
        ->th('SO Size:')
        ->th('Safety order step:')
        ->th('SO Volume:')
        ->th('SO Step scale:')
        ->th('Take profit %:')
        ->th('Trailing enabled?:')
        ->th('Trailing %:')
        ->th('Cooldown:')
        ->th('Leverage type')
        ->th('Leverage setting')
        ->th('Enabled?')
        ->th('Strategy');

  

    $table->tr('global_header')
        ->td('Change for all bots :' , '' , 'colspan=2')
        ->td(create_dropdown_options(['limit' , 'market'] , '' , 'so_type_all' , '' , 'limit'))
        ->td(create_input_number('' , 'max_so_all' , '' , ''))
        ->td(create_input_number('' , 'act_so_all' , '' , ''))
        ->td(create_dropdown_options(['fixed' , 'percentage'] , '' , 'size_type_all' , 'cross' , ''))
        ->td(create_input_float('' , 'bo_size_all' , '' , ''))
        ->td(create_input_float('' , 'so_size_all' , '' , ''))
        ->td(create_input_float('' , 'so_perc_all' , '' , ''))
        ->td(create_input_float('' , 'so_volume_all' , '' , ''))
        ->td(create_input_float('' , 'so_step_all' , '' , ''))
        ->td(create_input_float('' , 'tp_all' , '' , ''))
        ->td(create_dropdown_options([0,1] , '' , 'ttp_all' , '' , '1'))
        ->td(create_input_float('' , 'ttp_deviation_all' , '' , ''))
        ->td(create_input_number('' , 'cooldown_all' , '' , '0'))
        ->td(create_dropdown_options(['cross' , 'isolated'] , '' , 'lev_type_all' , 'cross' , ''))
        ->td(create_dropdown_number(0 , 125 , '' , 'lev_value_all' , '' , '20'))
        ->td(create_dropdown_options([0,1] , '' , 'is_enabled_all' , '' , '1'))
        ->td(create_customizable_dropdown_options(array_map(function ($n) {
            return ['value' => $n['id'], 'label' => $n['name']];
        }, $strategies), '', 'strategy_id_all', '', ['value' => '', 'label' => '']));

    
        
   
    foreach ((array)$bots as $bot) {

        if($bot['base_order_volume_type'] == 'percent') {
            $size_type = 'percentage';
        } else {
            $size_type = 'fixed';
        }

        // Find local strategy
        $local_bot = [];
        foreach ($local_bots as $b) {
            if ($b['account_bot_id'] == $bot['id']) {
                $local_bot = ['value' => $b['strategy_id'], 'label' => $b['strategy_name']];
                break;
            }
        }

        $table->tr()
            ->td($bot['id'].' ('.$bot['name'].')')
            ->td($bot['pairs'][0])
            ->td(create_dropdown_options(['limit' , 'market'] , 'so_type_bots_'.$bot['id'] , 'so_type_bots' , 'bot_'.$bot['id'] , $bot['start_order_type']))
            ->td(create_input_number('max_so_bots_'.$bot['id'] , 'max_so_bots' , 'bot_'.$bot['id'] , $bot['max_safety_orders']))
            ->td(create_input_number('act_so_bots_'.$bot['id'] , 'act_so_bots' , 'bot_'.$bot['id'] , $bot['active_safety_orders_count']))
            ->td(create_dropdown_options(['fixed' , 'percentage'] ,  'size_type_bots_'.$bot['id'] , 'size_type_bots' , 'bot_'.$bot['id'] , $size_type))
            ->td(create_input_float('bo_size_bots_'.$bot['id'] , 'bo_size_bots' , 'bot_'.$bot['id'] , $bot['base_order_volume']))
            ->td(create_input_float('so_size_bots_'.$bot['id'] , 'so_size_bots' , 'bot_'.$bot['id'] , $bot['safety_order_volume']))
            ->td(create_input_float('so_perc_bots_'.$bot['id'] , 'so_perc_bots' , 'bot_'.$bot['id'] , $bot['safety_order_step_percentage']))
            ->td(create_input_float('so_volume_bots_'.$bot['id'] , 'so_volume_bots' , 'bot_'.$bot['id'] , $bot['martingale_volume_coefficient']))
            ->td(create_input_float('so_step_bots_'.$bot['id'] , 'so_step_bots' , 'bot_'.$bot['id'] , $bot['martingale_step_coefficient'] ))
            ->td(create_input_float('tp_bots_'.$bot['id'] , 'tp_bots' , 'bot_'.$bot['id'] , $bot['take_profit'] , 0.05))
            ->td(create_dropdown_options([0,1] ,  'ttp_bots_'.$bot['id'] , 'ttp_bots' , 'bot_'.$bot['id'] , $bot['trailing_enabled']))
            ->td(create_input_float('ttp_deviation_bots_'.$bot['id'] , 'ttp_deviation_bots' , 'bot_'.$bot['id'] , $bot['trailing_deviation'] , 0.01))
            ->td(create_input_number('cooldown_bots_'.$bot['id'] , 'cooldown_bots' , 'bot_'.$bot['id'] , $bot['cooldown']))
            ->td(create_dropdown_options(['cross' , 'isolated'] ,  'lev_type_bots_'.$bot['id'] , 'lev_type_bots' , 'bot_'.$bot['id'] , $bot['leverage_type']))
            ->td(create_dropdown_number(0 , 125 ,  'lev_value_bots_'.$bot['id'] , 'lev_value_bots' , 'bot_'.$bot['id'] , $bot['leverage_custom_value']))
            ->td(create_dropdown_options([0 ,1] ,  'is_enabled_bots_'.$bot['id'] , 'is_enabled_bots' , 'bot_'.$bot['id'] , $bot['is_enabled']))
            ->td(create_customizable_dropdown_options(array_map(function ($n) {
                return ['value' => $n['id'], 'label' => $n['name']];
            }, $strategies), 'strategy_id_bots_'.$bot['id'], 'strategy_id_bots', 'bot_'.$bot['id'], $local_bot ?: ['value' => '', 'label' => '']));
    }

    echo $table->getTable();

    echo '</form>';

}

if($action == 'load_tv_alerts') {

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    $xcommas = new MC3Commas\threeCommas(BASE_URL , $account_info['api_key'] , $account_info['api_secret']); 
    $bots = $xcommas->get_all_bots(['account_id' => $account_info['bot_account_id'] , 'limit' => 100]);

    array_multisort(array_column((array)$bots, 'created_at'),  SORT_DESC , $bots);

    echo '<h2> Trading view alerts </h2>';

    echo '<h3> General alerts</h2>';

    $table = new STable();
    $table->class = 'table table-hover table-striped table-bordered';
    $table->width = '100%';

    $table->thead()
    ->th('Type:')
    ->th('Alert');



    $table->tr()
    ->td('Enable account')
    ->td(json_encode(['account_id' => (int)$account_info['bot_account_id'] , 'message' => 'enable_bots']  ));

    $table->tr()
    ->td('Disable account')
    ->td(json_encode(['account_id' => (int)$account_info['bot_account_id'] , 'message' => 'disable_bots']  ));

    $table->tr()
    ->td('Check open deals (uses Telegram)')
    ->td(json_encode(['account_id' => (int)$account_info['bot_account_id'] , 'message' => 'check_open_deals']  ));

    echo $table->getTable();

    $table = '';
         
    
    echo '<h3> Bot specific alerts</h2>';

    $table = new STable();
    $table->class = 'table table-hover table-striped table-bordered';
    $table->id = 'bot_spec_alerts';
    $table->width = '100%';

    $table->thead()
        ->th('Bot (name):')
        ->th('Created :')
        ->th('Pair :')
        ->th('Alert')
        ->th('Action');
    
   
    foreach ((array)$bots as $bot) {

        //$bot['id'] = '87654321';

        $result['account_id'] = (int)$account_info['bot_account_id'];
        $result['bot_id'] = (int)$bot['id'];
        $result['pair'] = $bot['pairs'][0];

        $table->tr()
            ->td($bot['id']. ' ('.$bot['name'].')')
            ->td($bot['created_at'])
            ->td($bot['pairs'][0])
            ->td('<span class="copy_text" id="alert_'.$bot['id'].'">'.json_encode($result).'</span>')
            ->td('<button class="btn btn-info copy_alert" data-clipboard-target="#alert_'.$bot['id'].'">
            <i class="fas fa-copy"></i>
        </button>');
    }

    echo $table->getTable();

}

if($action == 'load_telegram_settings') {

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    $settings = $dataReader->get_account_settings($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    if($settings['notify_telegram'] == 1) {
        $checked = 'checked';
    } else {
        $checked = '';
    }

    echo '<h2> Telegram settings </h2>';

    echo '<form method="POST" id="change_telegram_settings" onsubmit="return a_change_telegram_settings();">';

    echo '<input type="hidden" name="account_id" value="'.$internal_account_id.'"/>';

    echo '<div class="field">
            <label> Use telegram : </label>
            <input type="checkbox" name="notify_telegram" '.$checked.' id="notify_telegram" /> 
        </div>';

    echo '<div class="field">
        <label> Telegram bot hash : </label>
        <input type="text" name="telegram_bot_hash" value="'.$settings['telegram_bot_hash'].'" />
    </div>';

    echo '<div class="field">
        <label> Telegram Chat-ID  </label>
        <input type="text" name="telegram_chat_id" value="'.$settings['telegram_chat_id'].'" />
    </div>';
    
    echo '<input type="submit" value="Save settings" />';
    echo '</form>';

}

if($action == 'load_logbook') {

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);  

    $log_data = $dataReader->get_logbook($account_info['bot_account_id']);

    echo '<h2> Logbook </h2>';

    $table = new STable();
    $table->class = 'table table-hover table-striped table-bordered';
    $table->id = 'logbook_table';
    $table->width = '100%';

    $table->thead()
    ->th('Date / time :')
    ->th('Pair :')
    ->th('Message :');

    foreach($log_data as $log) {
        $table->tr()
        ->td($log['timestamp'])
        ->td($log['pair'])
        ->td($log['message']);

    }

    echo $table->getTable();
}

if($action == 'load_debuglog') {

    $log_data = $dataReader->get_debuglog();

    
    echo '<h2> Debug log </h2>';

    $table = new STable();
    $table->class = 'table table-hover table-striped table-bordered';
    $table->id = 'debug_table';
    $table->width = '100%';

    $table->thead()
    ->th('Date / time :')
    ->th('Jobs :')
    ->th('Alerts :')
    ->th('Calls to 3C :')
    ->th('Calls / Alerts :')
    ->th('Errors :')
    ->th('Avg. job time :')
    ->th('Max. job time :')
    ->th('Exceed 15 secs :')
    ->th('Exceed 30 secs :');

    foreach($log_data as $log) {
        $table->tr()
        ->td($log['time'])
        ->td($log['jobs'])
        ->td($log['alerts'])
        ->td($log['calls'])
        ->td($log['average_calls'])
        ->td($log['errors'])
        ->td($log['avg_job_time'])
        ->td($log['max_job_time'])
        ->td($log['exceed_15s'])
        ->td($log['exceed_30s']);

    }

    echo $table->getTable();
}

if($action == 'load_strategy') {

    $strategy_data = $dataReader->get_strategies();

    
    echo '<h2> Strategy Management </h2>';

    $table = new STable();
    $table->class = 'table table-hover table-striped table-bordered';
    $table->id = 'strategy_table';
    $table->width = '100%';

    $table->thead()
    ->th('ID :')
    ->th('Name :')
    ->th('Created By :')
    ->th('TV Alerts')
    ->th('Actions');

    foreach($strategy_data as $strategy) {
        $table->tr()
        ->td($strategy['id'])
        ->td($strategy['name'])
        ->td($strategy['created_by'])
        ->td('<a class="strategy_tv_alerts_link" data-id="'.$strategy['id'].'" id="strategy_'.$strategy['id'].'"><i class="fas fa-chart-bar"></i>  Trading View alerts</a>')
        ->td("<a class='edit_strategy_link' data-strategy='".json_encode($strategy)."'><i class='fas fa-edit'></i>&nbsp;Edit</a>&nbsp;
        <a class='delete_strategy_link' data-strategy='".json_encode($strategy)."'><i class='fas fa-trash'></i>&nbsp;Delete</a>");
    }

    echo $table->getTable();

    echo '<br />';
    echo '<i class="fas fa-plus"></i>  <a class="add_strategy_link" ac_id="1"> Add Strategy</a>';
}

if ($action == 'load_strategy_tv_alert') {
    $strategy = $dataReader->get_strategy($_REQUEST['strategy_id']);
    $pairs = $dataReader->load_pairs_from_local_bots();
    echo '<h2> TV Alerts ('.$_REQUEST['strategy_id'].' - '.$strategy['name'].') </h2>';

    $table = new STable();
    $table->class = 'table table-hover table-striped table-bordered';
    $table->id = 'strategy_tv_alert_table';
    $table->width = '100%';

    $table->thead()
    ->th('Pair :')
    ->th('Alert:')
    ->th('Action');

    foreach($pairs as $pair) {
        $result['strategy_id'] = (int)$strategy['id'];
        $exchange_pair = "USDT_".$pair['token'];;
        $result['pair'] = $exchange_pair;

        $table->tr()
        ->td($exchange_pair)
        ->td('<span class="copy_text" id="strategy_alert_'.$exchange_pair.'">'.json_encode($result).'</span>')
        ->td('<button class="btn btn-info copy_alert" data-clipboard-target="#strategy_alert_'.$exchange_pair.'">
            <i class="fas fa-copy"></i>
        </button>');
    }

    echo $table->getTable();
}

if ($action == 'get_strategy') {
    $res = $dataReader->get_strategy($_POST['id']);

    $html ='<h2> Edit strategy </h2>
               
    <form method="POST" id="edit_strategy_form" onsubmit="return a_edit_strategy();">
        <input type="hidden" name="action" id="action" value="edit_strategy" />
        <input type="hidden" name="id" value="'.$res['id'].'"
        <div class="field">
            <label> Name:  </label>
            <input type="text" name="name" id="name" value="'.$res['name'].'" /> 
        </div>
        <div class="field">
            <label> Description:  </label>
            <textarea name="description" id="description" rows="5">'.$res['description'].'</textarea>
        </div>
        <input type="submit" name="submit_form" value="Submit">
    </form>';

    echo $html;
}

/**
 * Add a strategy
 */
if($action == 'add_strategy') {

    $dataMapper->insert_strategy($_SESSION['user_id'] , $_REQUEST['name'] , $_REQUEST['description']);

    echo 'Strategy added.';
}

if($action == 'edit_strategy') {

    $dataMapper->edit_strategy($_REQUEST['id'] , $_REQUEST['name'] , $_REQUEST['description']);

    echo 'Strategy updated.';
}

if($action == 'delete_strategy') {

    $dataMapper->delete_strategy($_REQUEST['id']);

    echo 'Strategy deleted.';
}

if($action == 'sent_telegram_msg') {

    

    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    $settings = $dataReader->get_account_settings($internal_account_id);


    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    $telegram_bot_hash = $settings['telegram_bot_hash'];
    $telegram_chat_id = $settings['telegram_chat_id'];
    $msg = 'Account : '.$account_info['account_name'].'. This is a test message from Smart Simple Bot';

    telegram($telegram_bot_hash , $telegram_chat_id , $msg);

    echo 'Test message sent to Telegram. Check the response';

}


/**
 * Change all bots
 */
if($action == 'change_bots') {

    $internal_account_id = $_POST['account_id'];
    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    $post_data = $_POST;

    $keys = array_keys($post_data);

    // Get all the bot ids
    foreach ($keys as $key) {
        $splited = preg_split("/_/",$key);
        $bot_ids[] = end($splited);
    }

    $bot_ids = array_unique($bot_ids);



    $xcommas = new MC3Commas\threeCommas(BASE_URL , $account_info['api_key'] , $account_info['api_secret']); 
    $bots = $xcommas->get_all_bots(['account_id' => $account_info['bot_account_id'] , 'limit' => 100]);

    foreach ($bots as $bot) {

        // Double check if the bot id from 3commas is in the POST array
        if(in_array($bot['id'] , $bot_ids)) {

            if($_POST['size_type_bots_'.$bot['id']] == 'percentage') {
                $size_type = 'percent';
            } else {
                $size_type = 'quote_currency';
            }

            $data = [
                // General
                'name' => $bot['name'],
                'pairs' => json_encode($bot['pairs']),
                'start_order_type' => $_POST['so_type_bots_'.$bot['id']], 
                'strategy_list' => json_encode(['strategy' => $bot['strategy_list'][0]['strategy']]),
                'bot_id' => $bot['id'],
                'is_enabled' => $_POST['is_enabled_bots_'.$bot['id']],
                
                 // Volumes

                'base_order_volume' => $_POST['bo_size_bots_'.$bot['id']],
                'safety_order_volume' => $_POST['so_size_bots_'.$bot['id']],
                'safety_order_volume_type' => $size_type,
                'base_order_volume_type' => $size_type,

                // SO paramaters
                'safety_order_step_percentage' => $_POST['so_perc_bots_'.$bot['id']], 
                'max_safety_orders' => $_POST['max_so_bots_'.$bot['id']], 
                'active_safety_orders_count' => $_POST['act_so_bots_'.$bot['id']], 
                'martingale_volume_coefficient' => $_POST['so_volume_bots_'.$bot['id']], 
                'martingale_step_coefficient' => $_POST['so_step_bots_'.$bot['id']], 
                
                // Profit types
                'take_profit_type' => $bot['take_profit_type'],
                'take_profit' => $_POST['tp_bots_'.$bot['id']],
                'trailing_enabled' => $_POST['ttp_bots_'.$bot['id']], 
                'trailing_deviation' => $_POST['ttp_deviation_bots_'.$bot['id']], 

                'cooldown' => $_POST['cooldown_bots_'.$bot['id']], 

      

                // Leverage parameters
                'leverage_type' =>  $_POST['lev_type_bots_'.$bot['id']], //$bot['leverage_type'] ,
                'leverage_custom_value' => $_POST['lev_value_bots_'.$bot['id']], //$bot['leverage_custom_value'],
            ];
            
            if ($_POST['is_enabled_bots_'.$bot['id']] == "1") {
                $xcommas->enable_bot($bot['id']);
            } 
            if ($_POST['is_enabled_bots_'.$bot['id']] == "0") {
                $xcommas->disable_bot($bot['id']);
            }
                   

            $update = $xcommas->update_bot($bot['id'] , $data);
            // Update local DB strategy ID
            if ($_POST['strategy_id_bots_'.$bot['id']] !== '') {
                $dataMapper->update_local_bot($bot['id'], $_POST['strategy_id_bots_'.$bot['id']]);
            }
        }
    } 

    echo 'Bots updated. Please review your settings on 3Commas!';
}

// Change telegram settings
if($action == 'change_telegram_settings') {

    $internal_account_id = $_POST['account_id'];
    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    if($_POST['notify_telegram'] == 'on') {
        $notify = 1;
    } else {
        $notify = 0;
    }

    $dataMapper->update_telegram_settings($internal_account_id , $notify , $_POST['telegram_bot_hash'] , $_POST['telegram_chat_id']);

    echo 'Telegram settings updated.';
}

if ($action == 'cancel_all_deals') {
    $explode = explode('_' , $_REQUEST['id']);
    $internal_account_id = $explode[1];

    $account_info = $dataReader->get_account_info_internal($internal_account_id);

    // Terminate if the user is nog logged in
    check_credentials($account_info['user_id']);

    $xcommas = new MC3Commas\threeCommas(BASE_URL , $account_info['api_key'] , $account_info['api_secret']);
    $bots = $xcommas->get_all_bots(['account_id' => $account_info['bot_account_id'] , 'limit' => 100]);
    foreach ((array)$bots as $bot) {
        $xcommas->cancel_all_deals($bot['id']);
    }

    echo 'All deals cancelled for Account: '.$account_info['account_name'];
}

if ($action == 'refetch_bots') {
    // Fetch all bots from 3Commas & populate them in our local DB.
    $all_accounts = $dataReader->get_all_accounts();
    foreach($all_accounts as $account_wrapper) {
        $account_info = $dataReader->get_account_info_internal($account_wrapper['internal_account_id']);
        $xcommas = new MC3Commas\threeCommas(BASE_URL , $account_info['api_key'] , $account_info['api_secret']);
        $bots = $xcommas->get_all_bots(['account_id' => $account_info['bot_account_id'] , 'limit' => 100]);

        foreach ((array)$bots as $bot) {
            $update_or_create_bot = '
                INSERT INTO bots (account_bot_id, account_id, name, pairs, enabled, last_updated) 
                VALUES (:account_bot_id, :account_id, :name, :pairs, :enabled, current_timestamp()) 
                ON DUPLICATE KEY UPDATE
                account_id = :account_id, name = :name, pairs = :pairs, enabled = :enabled
            ';
            $stmt = $dataMapper->dbh->prepare($update_or_create_bot);
            $stmt->bindParam(':account_id', $account_info['bot_account_id']);
            $stmt->bindParam(':account_bot_id', $bot['id']);
            $stmt->bindParam(':name', $bot['name']);
            $stmt->bindParam(':pairs', $bot['pairs'][0]);
            $enabled = $bot['is_enabled'] == 1 ? 1 : 0;
            $stmt->bindParam(':enabled', $enabled);
            $stmt->execute();
            $stmt = null;
        }
    }

    echo "Loaded latest 3Commas bots into local DB.";
}

if ($action == 'get_indicator') {
    $res = $dataReader->get_indicator($_POST['id']);

    $html ='<h2> Edit Indicator </h2>
               
    <form method="POST" id="edit_indicator_form" onsubmit="return a_edit_indicator();">
        <input type="hidden" name="action" id="action" value="edit_indicator" />
        <input type="hidden" name="id" value="'.$res['id'].'"
        <div class="field">
            <label> Name:  </label>
            <input type="text" name="name" id="name" value="'.$res['name'].'" /> 
        </div>
        <input type="submit" name="submit_form" value="Submit">
    </form>';

    echo $html;
}

if($action == 'load_indicators') {

    $indicator_data = $dataReader->load_indicators();
    
    echo '<h2> Indicators </h2>';

    $table = new STable();
    $table->class = 'table table-hover table-striped table-bordered';
    $table->id = 'indicator_table';
    $table->width = '100%';

    $table->thead()
        ->th('ID :')
        ->th('Name :')
        ->th('Created By :')
        ->th('TV Alerts')
        ->th('Actions');

    foreach($indicator_data as $indicator) {
        $timeframe_dropdown = '<select class="timeframe-dropdown" data-id="'.$indicator['id'].'">
            <option value="15m">15m</option>
            <option value="30m">30m</option>
            <option value="1h">1h</option>
            <option value="4h">4h</option>
            <option value="1d">1d</option>
        </select>';
        $table->tr()
        ->td($indicator['id'])
        ->td($indicator['name'])
        ->td($indicator['created_by'])
        ->td('Timeframe: '.$timeframe_dropdown.'<br /><a class="indicator_tv_alerts_link" data-id="'.$indicator['id'].'" id="indicator_'.$indicator['id'].'"><i class="fas fa-chart-bar"></i>  Trading View alerts</a>')
        ->td("<a class='edit_indicator_link' data-indicator='".json_encode($indicator)."'><i class='fas fa-edit'></i>&nbsp;Edit</a>&nbsp;
        <a class='delete_indicator_link' data-indicator='".json_encode($indicator)."'><i class='fas fa-trash'></i>&nbsp;Delete</a>");
    }

    echo $table->getTable();

    echo '<br />';
    echo '<i class="fas fa-plus"></i>  <a class="add_indicator_link" ac_id="1"> Add Indicator</a>';
}

if($action == 'add_indicator') {

    $dataMapper->insert_indicator($_SESSION['user_id'] , $_REQUEST['name']);

    echo 'Indicator added.';
}

if($action == 'edit_indicator') {

    $dataMapper->edit_indicator($_REQUEST['id'] , $_REQUEST['name']);

    echo 'Indicator updated.';
}

if($action == 'delete_indicator') {

    $dataMapper->delete_indicator($_REQUEST['id']);

    echo 'Indicator deleted.';
}

if ($action == 'load_indicator_tv_alert') {
    $indicator_id = $_REQUEST['indicator_id'];

    $indicator = $dataReader->get_indicator($indicator_id);

    $timeframe = $_REQUEST['timeframe'];
    $pairs = $dataReader->load_pairs_from_local_bots();
    echo '<h2> TV Alerts ('.$indicator_id.' - '.$indicator['name'].') </h2>';

    $table = new STable();
    $table->class = 'table table-hover table-striped table-bordered';
    $table->id = 'indicator_tv_alert_table';
    $table->width = '100%';

    $table->thead()
    ->th('Pair :')
    ->th('Bullish/Buy Alert:')
    ->th('Copy Buy Alert')
    ->th('Bearish/Sell Alert')
    ->th('Copy Sell Alert');

    foreach($pairs as $pair) {
        $exchange_pair = "USDT_".$pair['token'];;
        $buy_result['pair'] = $exchange_pair;
        $buy_result['indicator_id'] = $indicator_id;
        $buy_result['timeframe'] = $timeframe;
        $buy_result['buy_signal'] = 1;
        $buy_result['sell_signal'] = 0;

        $sell_result['pair'] = $exchange_pair;
        $sell_result['indicator_id'] = $indicator_id;
        $sell_result['timeframe'] = $timeframe;
        $sell_result['buy_signal'] = 0;
        $sell_result['sell_signal'] = 1;

        $table->tr()
        ->td($exchange_pair)
        ->td('<span class="copy_text" id="indicator_buy_alert_'.$exchange_pair.'">'.json_encode($buy_result).'</span>')
        ->td('<button class="btn btn-success copy_alert" data-clipboard-target="#indicator_buy_alert_'.$exchange_pair.'">
            <i class="fas fa-copy"></i>
        </button>')
        ->td('<span class="copy_text" id="indicator_sell_alert_'.$exchange_pair.'">'.json_encode($sell_result).'</span>')
        ->td('<button class="btn btn-danger copy_alert" data-clipboard-target="#indicator_sell_alert_'.$exchange_pair.'">
            <i class="fas fa-copy"></i>
        </button>');
    }

    echo $table->getTable();
}

if ($action == 'load-log') {
    $file_name = $_REQUEST['file_name'];

    $file = fopen("../logs/$file_name", "r") or die("Unable to open file!");
    echo '<pre class="log-trace">'.fread($file, filesize("../logs/$file_name")).'</pre>';
    fclose($file);
}
?> 
