<?php
error_reporting(E_ALL);
// We need to use sessions, so you should always start sessions using the below code.
session_start();

// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
	header('Location: index.php?response=notloggedin');
	die;
}

$files = array_diff(scandir('../logs'), array('.', '..'));

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<title>Smart Simple Bot</title>
		
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.0.0/jq-3.2.1/dt-1.10.16/r-2.2.1/datatables.min.css"/>
		<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
        <link href="css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.0.0/jq-3.2.1/dt-1.10.16/r-2.2.1/datatables.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>	
		<script type="text/javascript" src="js/ajax.js"></script>
		<script>
		$(document).ready(function(){
			$('.open-log-file').on('click', function() {
				$('#log-data').show();
				var filename = $(this).data('name');
				$('#log-name').html(filename);
				$.ajax({
					type: 'post',
					url: 'requesthandler.php?action=load-log',
					data: {
						file_name: filename,
					},
					success: function (response) {
						if (response == 'ERROR_NOT_LOGGED_IN') {
							location.href = 'logout.php?response=incorrect_ajax_call';
						} else {
							$('#log').html(response);
						}
					}
				});
			});
		});	
		</script>
	</head>
	
	<body class="loggedin">
		<div class="content">
			
            <h2>View Logs</h2>
        
            <div class="home"><a class="back_home_link" href="admin_home.php"><i class="fas fa-home"></i> Back to home</a></div>
       
            <div class="workspace">

                <div id="logs-by-date">
                    <?php
                        foreach ($files as $file) {
                            echo "<p><a class='open-log-file' data-name='$file'><i class='fas fa-book'></i> $file</a></p>";
                        }
                    ?>
                </div>
            </div>
                  

			<div class="workspace hide" id="log-data">
				<div class="logs">
					<h2 id="log-name"></h2>
				</div>
				<div id="log">

				</div>
			</div>
		</div>
	</body>
</html>