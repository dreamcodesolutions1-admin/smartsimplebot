<?php

// If using Sentry & Log, must include this first
include ('vendor/autoload.php');

include ('app/Sentry.php');

ini_set('display_errors', 1);
error_reporting(E_ALL);

function parseToken($token) {
    $handle = curl_init();

    $url = "https://open-api.coinglass.com/api/pro/v1/futures/funding_rates_chart?type=U&symbol=".$token;
 
    // Set the url
    curl_setopt($handle, CURLOPT_URL, $url);
    // Set the result output to be a string.
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_HTTPHEADER, array(
        'coinglassSecret: 3f940e2da26f4401b57862a183d52f59'
    ));
    
    $output = curl_exec($handle);
    
    curl_close($handle);

    $json = json_decode($output);

    if ($json->success) {
        $data = $json->data->dataMap;
        $result = ['token' => $token, 'exchange' => []];

        if (isset($data->Binance)) {
            array_push($result['exchange'], ['name' => 'Binance', 'rate' => $data->Binance[count($data->Binance) - 1]]);
        }
        if (isset($data->Bybit)) {
            array_push($result['exchange'], ['name' => 'Bybit', 'rate' => $data->Bybit[count($data->Bybit) - 1]]);
        }
        if (isset($data->Okex)) {
            array_push($result['exchange'], ['name' => 'Okex', 'rate' => $data->Okex[count($data->Okex) - 1]]);
        }
        return $result;
    }
    else {
        echo "Data source error.";
    }
}

$tokens = ['BTC','ETH','EOS','BCH','LTC','XRP','DOT','ADA','BNB','COMP', 'LINK'];

$result = [];

$date = date('Y/m/d h:i:sa');

foreach ($tokens as $token) {
    array_push($result, parseToken($token));
}

$markdown = "📊 <b>Funding Rate Update</b>\n🗓 <b>".$date." UTC</b>";

$alert_markdown = "";

$json = json_decode(json_encode($result));

foreach ($json as $token) {
    foreach ($token->exchange as $exchange) {
        if ($token->token == 'BTC') {
            if ($exchange->rate > 0.03) {
                $alert_markdown .= "\n".$token->token.": <b>".$exchange->rate."%</b> (".$exchange->name.")";
            }
        }
        else {
            if ($exchange->rate > 0.06) {
                $alert_markdown .= "\n".$token->token.": <b>".$exchange->rate."%</b> (".$exchange->name.")";
            }
        }
    }
}

if ($alert_markdown != "") {
    $markdown .= "\n\n⚠️ Tokens to be cautious: ".$alert_markdown;
}
else {
    $markdown .= "\n\n✅ All Tokens are healthy.";
}

foreach ($json as $token) {
    $markdown .= "\n\n<code><b>".$token->token."</b></code>";
    foreach ($token->exchange as $exchange) {
        $formatted_rate = $exchange->rate;
        if ($token->token == 'BTC') {
            if ($exchange->rate > 0.03) {
                $markdown .= "\n".$exchange->name.": <b>".$formatted_rate."%</b>";
            }
            else {
                $markdown .= "\n".$exchange->name.": ".$formatted_rate."%";
            }
        }
        else {
            if ($exchange->rate > 0.06) {
                $markdown .= "\n".$exchange->name.": <b>".$formatted_rate."%</b>";
            }
            else {
                $markdown .= "\n".$exchange->name.": ".$formatted_rate."%";
            }
        }
    }
}

try {
    $handle = curl_init('https://api.telegram.org/bot2143354062:AAGFW5xaysv5x4Z_JnwZE1M2Zc2-76JFgpg/sendMessage');
    // Set the result output to be a string.
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_POSTFIELDS, array(
        'chat_id' => -588936194,
        'parse_mode' => 'HTML',
        'text' => $markdown
    ));

    $output = curl_exec($handle);

    curl_close($handle);

    echo $output;
}
catch (Exception $e) {
    echo $e->getMessage();
}

?>