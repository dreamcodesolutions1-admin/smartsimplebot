<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

// If using Sentry & Log, must include this first
include ('vendor/autoload.php');

include ('app/Sentry.php');
include ('app/Log.php');
include ('app/Config.php');
include ('app/Core.php');
include ('app/3CommasConnector.php');
include ('app/DataMapper.php');
include ('app/DataReader.php');
include ('app/functions.php');

$dataMapper = new DataMapper();
$dataReader = new DataReader();
$logger = new EventLog();

$start = new DateTime();
echo 'TV Screener script started: '.$start->format('Y-m-d H:i:s').'<br />';

$all_accounts = $dataReader->get_all_accounts();

foreach($all_accounts as $account_wrapper) {
    $account_info = $dataReader->get_account_info($account_wrapper['bot_account_id']);
    $account_settings = $dataReader->get_account_settings($account_info['internal_account_id']);

    /**
     * 
     * Check if account exist , if not we can skip this iteration
     * 
     */
    if(!$account_info) {
        echo 'Account not found...<br />';
        continue; 
    }

    if ($account_settings['active'] == 0) {
        echo 'Account '.$account_info['bot_account_id'].' is not enabled. Skipping.<br />';
    }

    if ($account_settings['tv_screener_timeframe'] == "") {
        echo 'TV Screener is disabled for account '.$account_wrapper['bot_account_id'].'. Skipping. <br />';
        continue;
    }

    echo "Running for account ".$account_wrapper['bot_account_id'],':<br />';

    $screener_logs = $dataReader->load_screener_log($account_settings['tv_screener_timeframe']);
    $bots = $dataReader->load_local_bots($account_wrapper['bot_account_id']);

    $xcommas_main = new MC3Commas\threeCommas(BASE_URL , $account_info['api_key'] , $account_info['api_secret']);
    foreach ($screener_logs as $log) {
        // Find bot by pairs
        $pair1 = 'USDT_'.str_replace('USDT', '', $log['pair']);
        $pair2 = 'USDT_'.$log['pair'];
        $pair3 = 'USD_'.str_replace('USDT', '', $log['pair']).'-PERP';
        foreach ($bots as $bot) {
            if (in_array($bot['pairs'], [$pair1, $pair2, $pair3])) {
                if ($log['recommendation'] > 0) {
                    if ($bot['enabled'] == 0) {
                        $log_message = [
                            'timeframe' => $account_settings['tv_screener_timeframe'],
                            'bot_id' => $bot['account_bot_id'],
                            'enabled' => 1,
                            'pair' => $bot['pairs'],
                        ];
                        $logger->logEvent('bot', 'Enabled Bot', $log_message);
                        if (!DEBUG) {
                            $dataMapper->toggle_bot($bot['account_bot_id'], 1);
                            $xcommas_main->enable_bot($bot['account_bot_id']);
                        }
                        
                    }
                }
                else if ($log['recommendation'] < 0) {
                    if ($bot['enabled'] == 1) {
                        echo 'Disabled: '.$bot['pairs'].'<br />';
                        $log_message = [
                            'timeframe' => $account_settings['tv_screener_timeframe'],
                            'bot_id' => $bot['account_bot_id'],
                            'enabled' => 0,
                            'pair' => $bot['pairs'],
                        ];
                        $logger->logEvent('bot', 'Disabled Bot', $log_message);
                        if (!DEBUG) {
                            $dataMapper->toggle_bot($bot['account_bot_id'], 0);
                            $xcommas_main->disable_bot($bot['account_bot_id']);
                        }
                    }
                }
            }
        }
        
    }
}

echo 'TV Screener script completed in: '.$start->diff(new DateTime())->s.' seconds.';

?>