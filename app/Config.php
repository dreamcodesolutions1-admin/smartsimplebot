<?php
/**
 * Set your own credentials for mysql/db
 */
define ('DB_HOST' , 'localhost');
define ('DB_DBNAME' , 'smartsimplebot');
define ('DB_CHARSET' , 'utf8mb4');
define ('DB_USERNAME' , 'root');
define ('DB_PASSWORD' , 'T0nyV00n@dc');

// Set this to true to prevent 3Commas from executing real orders.
define ('DEBUG', false);

define ('MAX_TIME_TO_CHECK_ALERT' , 35); // Set the max history in seconds to process an alert with the processor. Make sure this timescale is larger then frequency you call the alert_processor.
