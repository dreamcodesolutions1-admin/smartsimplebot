<?php

class DataReader extends Core
{

    public function get_account_info($bot_account_id) {

        try{
               
            $stmt = $this->dbh->prepare('SELECT * FROM accounts WHERE bot_account_id = :bot_account_id');
            $stmt->bindParam(':bot_account_id', $bot_account_id);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    
    }

    public function get_account_info_internal($internal_account_id) {

        try{
               
            $stmt = $this->dbh->prepare('SELECT * FROM accounts WHERE internal_account_id = :internal_account_id');
            $stmt->bindParam(':internal_account_id', $internal_account_id);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    
    }

    public function get_account_settings($internal_account_id) {

        try{

            $stmt = $this->dbh->prepare('SELECT * FROM account_settings WHERE internal_account_id = :internal_account_id');
            $stmt->bindParam(':internal_account_id', $internal_account_id);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    
    }

    public function get_user_credentials($user_name) {

        try{

            $stmt = $this->dbh->prepare('SELECT * FROM users WHERE user_name = :user_name');
            $stmt->bindParam(':user_name', $user_name);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    

    }

    public function get_user_accounts($user_id) {

        try{

            $stmt = $this->dbh->prepare('SELECT * FROM accounts WHERE user_id = :user_id');
            $stmt->bindParam(':user_id', $user_id);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    

    }

    public function get_all_accounts() {

        try{

            $stmt = $this->dbh->prepare('SELECT internal_account_id , bot_account_id FROM accounts');
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    

    }

    public function get_unprocessed_alerts($seconds_passed) {

        // Only fetch result not older then current_time - $seconds_passed seconds
        $start_time = date('Y-m-d H:i:s',time() - $seconds_passed);

        try{

            $stmt = $this->dbh->prepare('SELECT * , CASE WHEN input like \'%message%\' THEN 1 ELSE 2 END prio FROM raw_tv_input WHERE processed = :processed AND file_name = :file_name AND timestamp >= :start_time ORDER BY prio ASC');
            $stmt->bindValue(':processed', 0);
            $stmt->bindValue(':file_name', 'alert_handler.php');
            $stmt->bindParam(':start_time', $start_time);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    
    }

    public function get_logbook($bot_account_id) {

        $start_time = date('Y-m-d H:i:s',time() - (60 * 60 * 24 * 7));

        try{

            $stmt = $this->dbh->prepare('SELECT * FROM logbook WHERE account_id = :bot_account_id AND timestamp >= :start_time ORDER BY log_id');
            $stmt->bindParam(':bot_account_id', $bot_account_id);
            $stmt->bindParam(':start_time', $start_time);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    
    }

    public function get_debuglog() {

        $start_time = date('Y-m-d H:i:s',time() - (60 * 60 * 24 * 7));

        try{

            $stmt = $this->dbh->prepare('SELECT * FROM debug_calls WHERE time >= :start_time ORDER BY time DESC');
            $stmt->bindParam(':start_time', $start_time);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    
    }

    public function get_strategies() {

        try{

            $stmt = $this->dbh->prepare('
                SELECT A.strategy_id as id, A.name as name, A.description as description, B.user_name as created_by, A.last_updated as last_updated 
                FROM strategies A join users B on A.created_by = B.user_id
                WHERE A.active = 1');
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    
    }

    public function get_strategy($id) {

        try{

            $stmt = $this->dbh->prepare('
                SELECT A.strategy_id as id, A.name as name, A.description as description, B.user_name as created_by, A.last_updated as last_updated 
                FROM strategies A join users B on A.created_by = B.user_id
                WHERE A.strategy_id = :id');
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result[0];
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    
    }

    public function load_local_bots($account_id) {
        try{

            $stmt = $this->dbh->prepare('
                SELECT A.account_bot_id, A.account_id, A.strategy_id, B.name as strategy_name, A.pairs, A.enabled
                FROM bots A
                INNER JOIN strategies B on A.strategy_id = B.strategy_id 
                WHERE A.account_id = :account_id');
            $stmt->bindParam(':account_id', $account_id);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        } 
    }

    public function find_local_bots($account_id, $strategy_id, $pairs) {
        try{

            $stmt = $this->dbh->prepare('
                SELECT A.account_bot_id, A.account_id, A.strategy_id, B.name as strategy_name
                FROM bots A
                INNER JOIN strategies B on A.strategy_id = B.strategy_id 
                WHERE A.strategy_id = :strategy_id AND A.account_id = :account_id AND A.pairs = :pairs AND B.active = 1');
            $stmt->bindParam(':account_id', $account_id);
            $stmt->bindParam(':strategy_id', $strategy_id);
            $stmt->bindParam(':pairs', $pairs);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        } 
    }

    public function find_local_bots_by_strategy($strategy_id, $pairs) {
        try{
            $stmt = $this->dbh->prepare('
                SELECT A.account_bot_id, A.account_id, A.strategy_id, B.name as strategy_name
                FROM bots A
                INNER JOIN strategies B on A.strategy_id = B.strategy_id 
                WHERE A.strategy_id = :strategy_id AND (LOCATE(:usd, A.pairs) > 0 OR LOCATE("USD", A.pairs) > 0) 
                AND LOCATE(:token, A.pairs) > 0 AND B.active = 1');
            $stmt->bindParam(':strategy_id', $strategy_id);
            $stmt->bindParam(':usd', $pairs[0]);
            $stmt->bindParam(':token', $pairs[1]);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        } 
    }

    public function load_pairs_from_local_bots() {
        try{

            $stmt = $this->dbh->prepare("SELECT DISTINCT REGEXP_REPLACE(pairs, '(USD[T_]_?)|(-PERP)', '') as token from bots ORDER by token ASC");
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        } 
    }

    public function find_local_bot_by_pair($account_id, $pair) {
        try{
            $stmt = $this->dbh->prepare('SELECT account_bot_id, account_id, strategy_id FROM bots where pairs = :pairs');
            $stmt->bindParam(':account_id', $account_id);
            $stmt->bindParam(':pair', $pair);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        } 
    }

    public function load_screener_log($timeframe) {
        try{
            $stmt = $this->dbh->prepare('
                SELECT t1.* FROM tv_screener_logs t1
                WHERE t1.timeframe = :timeframe AND t1.timestamp = (SELECT MAX(t2.timestamp) FROM tv_screener_logs t2
                WHERE t2.pair = t1.pair AND t2.timeframe = :timeframe GROUP BY t2.pair, t2.timeframe)
                ORDER BY t1.pair ASC');
            $stmt->bindParam(':timeframe', $timeframe);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }
    }

    public function load_screener_log_data($log_id) {
        try{
            $stmt = $this->dbh->prepare("SELECT * FROM tv_screener_data_logs WHERE screener_id = :log_id ORDER by timestamp DESC, pair ASC");
            $stmt->bindParam(':log_id', $log_id);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }
    }

    public function load_indicators() {
        try{

            $stmt = $this->dbh->prepare('
                SELECT A.indicator_id as id, A.name as name, B.user_name as created_by, A.last_updated as last_updated 
                FROM indicators A join users B on A.created_by = B.user_id
                WHERE A.active = 1');
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }   
    }

    public function get_indicator($id) {
        try{

            $stmt = $this->dbh->prepare('
                SELECT A.indicator_id as id, A.name as name, B.user_name as created_by, A.last_updated as last_updated 
                FROM indicators A join users B on A.created_by = B.user_id
                WHERE A.indicator_id = :id ');
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result[0];
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }    
    }

    public function load_indicators_by_name() {
        try{
            $stmt = $this->dbh->prepare("SELECT DISTINCT name FROM indicator_logs");
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $stmt = null;

            return $result;
        }
        catch (PDOExecption $e){
            echo $e->getMessage();
        }
    }
}