<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class EventLog {
    // event type: Open/Close Bot
    public function logEvent($eventType, $message, $payload) {
        
        $logger = new Logger($eventType);
        $logger->pushHandler(new StreamHandler("logs/$eventType"."_".date("Ymd").".log", Logger::INFO));
        
        $logger->info($message, $payload);
    }
}

?>